
// MapLibre example

const mapLibreMap = new maplibregl.Map({
    container: 'mapLibreMap',
    style: './style.json', // stylesheet location
    center: [5.286758, 51.879391], // starting position [lng, lat]
    zoom: 15, // starting zoom
    hash: true
});


mapLibreMap.on('load', function () {

    // add source
    mapLibreMap.addSource('bag', {
        type: 'vector',
        url:
            //'https://files.commondatafactory.nl/tiles/epanden_20210406/epanden.json',
	    'http://127.0.0.1:8000/bagpanden.json',
        minzoom: 12,
        maxzoom: 15,
    });

    // add layer 2D layer
    // mapLibreMap.addLayer({
    //     id: 'mijnBaggerLaag2D',
    //     type: 'fill',
    //     source: 'bag',
    //     'source-layer': 'panden',
    //     minzoom: 12,
    //     maxzoom: 22,
    //     filter: ['has', 'woonfunctie_score'],
    //     paint: {
    //         'fill-color': [
    //             'case',
    //             ['has', 'woonfunctie_score'],
    //             [
    //                 'interpolate',
    //                 ['linear'],
    //                 ['get', 'woonfunctie_score'],
    //                 0,
    //                 '#EDE3CD',
    //                 100,
    //                 '#E0E0E0',
    //             ],
    //             '#E0E0E0',
    //         ]
    //     },
    // }, 'adres-labels'); //insert before layer


    // add layer 3D layer
    mapLibreMap.addLayer({
        id: 'mijnBaggerLaag2.5D',
        type: 'fill-extrusion',
        source: 'bag',
        'source-layer': 'bagvector.lod22_2d',
        minzoom: 12,
        maxzoom: 22,
        filter: ['has', 'h_dak_50p'],
        paint: {
            'fill-extrusion-color': [
                'case',
                ['has', 'h_dak_50p'],
                [
                    'interpolate',
                    ['linear'],
                    ['get', 'h_dak_50p'],
                    0,
                    '#EDE3CD',
                    50,
                    '#E0E0E0',
                ],
                '#E0E0E0',
            ],

            'fill-extrusion-height': [
                'interpolate',
                ['linear'],
                ['zoom'],
                13,
                0,
                16,
                ['*', 1.5, ['ceil', ['get', 'h_dak_50p']]],
            ],
        },
    }, 'adres-labels'); //insert before layer

})
